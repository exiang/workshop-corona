-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here
-- first u need to declare the sheet data, mainly to tell corona about your sprite image format
local sheetData = { width=162, height=214, numFrames=4, sheetContentWidth=648, sheetContentHeight=214 }
-- then you create the sheet object, where you use the sheetData you specified above as parameters to create it
local imageSheet = graphics.newImageSheet( "sprite.jpg", sheetData )

local sequenceData =
{
    name="cycling",
    start=1,
    count=4,
    time=1000,
    loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
    loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
}

local loadingSprite = display.newSprite( imageSheet, sequenceData )
loadingSprite.anchorX=0.5
loadingSprite.anchorY=0.5
loadingSprite.x = display.contentCenterX
loadingSprite.y = display.contentCenterY


loadingSprite:play()